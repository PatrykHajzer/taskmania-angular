import { Component, OnInit } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";
import {User} from "../model/User";
import {ApiService} from "../api/api.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup( {
    email: new  FormControl(),
    username: new FormControl(),
    password: new FormControl(),
  });

  constructor(
    private httpClient: HttpClient,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    const user = new User(
      this.registerForm.controls.username.value,
      this.registerForm.controls.password.value,
      this.registerForm.controls.email.value
      );
    this.apiService.register(user).subscribe();
  }
}
