import { Injectable } from "@angular/core";
import {User} from "../model/User";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private http: HttpClient;
  constructor() {}
  private userUrl;

  public createUser(user: User) {
    this.userUrl = "http://localhost:8080/login";
    return this.http.post<User>(this.userUrl, user);
  }
  public loginUser(user: User) {
    this.userUrl = "http://localhost:8080/register";
    return this.http.post<User>(this.userUrl, user);
  }}
