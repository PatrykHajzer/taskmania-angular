import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import {routing} from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "../login/login.component";
import { HomeComponent } from "../home/home.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MaterialsModule} from "./materials/materials.module";
import { UserComponent } from "../user/user.component";
import { RegisterComponent } from "../register/register.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    UserComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    MaterialsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
