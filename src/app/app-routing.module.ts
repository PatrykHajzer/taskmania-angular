
import { Routes, RouterModule } from "@angular/router";
import {LoginComponent} from "../login/login.component";
import {RegisterComponent} from "../register/register.component";
import {UserComponent} from "../user/user.component";
import {HomeComponent} from "../home/home.component";
import {AppComponent} from "./app.component";

const appRoutes: Routes = [
  // { path: "", redirectTo: "/", pathMatch: "full"},
  { path: "", component: HomeComponent},
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent},
  { path: "user", component: UserComponent},
  { path: "**", redirectTo: "" }
];

export const routing = RouterModule.forRoot(appRoutes);
