export class Task
{
  id: number;
  taskName: string;
  descr: string;
  constructor(taskName: string, descr: string) {
    this.taskName = taskName;
    this.descr = descr;
  }
}
