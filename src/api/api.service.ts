import { Injectable } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Task} from "../model/Task";
import {User} from "../model/User";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  // public firstPage: string = '';
  // public prevPage: string = '';
  // public nextPage: string = '';
  // public lastPage: string = '';

  protectedApiURL: string = "http://localhost:8080/tasks";
  unProtectedApiURL: string = "http://localhost:8080";
  constructor(private httpClient: HttpClient) { }

  public createTask(task: Task) {
    return this.httpClient.post(`${this.protectedApiURL}/tasks/`, task);
  }

  public showTask(id: number) {
    return this.httpClient.get(`${this.protectedApiURL}/tasks/${id}`);
  }

  public editTask(task: Task) {
    return this.httpClient.put(`${this.protectedApiURL}/tasks/${task.id}`, task);
  }

  public deleteTask(id: number) {
    return this.httpClient.delete(`${this.protectedApiURL}/tasks/${id}`);
  }

  public getTasks(url?: string) {
    return this.httpClient.get<Task[]>(`${this.protectedApiURL}/tasks`);
  }

  public login(user: User) {
    return this.httpClient.post(`${this.unProtectedApiURL}/login`, user);
  }

  public register(user: User) {
    return this.httpClient.post(`${this.unProtectedApiURL}/register`, user);
  }
}
