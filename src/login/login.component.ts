import { Component, OnInit } from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {User} from "../model/User";

import {ApiService} from "../api/api.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup ({
    username: new FormControl(),
    password: new FormControl(),
  });
  constructor(

    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private apiService: ApiService
  ) { }
  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const user = new User(
      this.loginForm.controls.username.value,
      this.loginForm.controls.password.value,
    )
    this.apiService.login(user).subscribe();
  }

  ngOnInit() {}
}
