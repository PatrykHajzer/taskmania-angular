import { Component, OnInit } from "@angular/core";
import {ApiService} from "../api/api.service";
import {Task} from "../model/Task";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent implements OnInit {

  tasks: Task[];
  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.getTasks().subscribe(
      res => this.handleSuccessfulResponse(res)
    );
  }
  handleSuccessfulResponse(response) {
    this.tasks = response;
  }

}
